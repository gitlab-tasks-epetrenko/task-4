1. We need to clone repository to have a local copy of code to work on:
- Open https://gitlab.com/gitlab-tasks-epetrenko/task-4

- Find a button clone and copy git link, then use git clone command to download a copy of repository:
- git clone git@gitlab.com:gitlab-tasks-epetrenko/task-4.git

2. To do a next changes - we should create our own branch, where we'll make our changes:
- git checkout -b 'Our-awesome-feature-branch'

3. We are ready to do our own changes!
- Gitlab maintains a list of files its tracking for changes, by default it contains all downloaded files in repository - simply speaking, it has a list of files in repository with their hashes.

- If you added any new files - you have to tell git to start tracking it - you need to update index to track it:
- git add <path/to/file>

- At any time you can compare status of your own copy with a git internal index - just issue command git status.

- git status will show a list of changed / modified files

4. Once your changes are ready - you need to commit all your changes - usually it can be done by issuing command
- git commit --all -m 'Commit message text'

5. Assuming we have an access to original repository, we can now push changes to the remote server
We can now push our changes to the original server with next command:
- git push --upstream origin Our-awesome-feature-branch
- We are telling git to upload our changes to server listed as origin in our upstream list, under the branch name Our-awesome-feature-branch

6. Last step in case of gitlab will be to create your a merge request.

- Open repository page, open merge requests and create new merge request stating source branch and target branch - source branch will be added to target
- https://gitlab.com/gitlab-tasks-epetrenko/task-4/-/merge_requests

Please remember to write a good description for your merge request, to let maintainers know the idea about code changes

7. Once you submitted your merge request repository owner should be notified, they will review your merge request and either let you know if any other changes needed or simply merge your code with a codebase.
